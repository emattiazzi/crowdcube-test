import React, {Component} from 'react';
import Container from '../Container/Container';

import './Filters.css';

class Filters extends Component {
  state = {
    showAll: false
  }

  render() {
    return (
      <div className="Filters">
        <Container>
          <div className="Filters__wrapper">
            <h2 className="Filter__title">Investment Opportunities</h2>
            <form className="">
              <div className="Filter__inputWrap">
                {/* TODO: should create custom componets but I don't have time  */}
                <label className="Filter__label" htmlFor="search">Search</label>
                <input className="Filter__input" type="text" placeholder="Search" />
                <input className="Filter__button" type="button" value="Search" />
              </div>
            </form>
          </div>
        </Container>
      </div>
    )
  }
}
export default Filters;
