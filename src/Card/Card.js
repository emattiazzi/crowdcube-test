import React from 'react';
import './Card.css';
import Progress from './Progress/Progress';
import Stats from './Stats/Stats';

const calculatePercentage = (target, reached) =>
  Math.floor(reached / target * 100);
const Card = props => {
  const style = {
    backgroundImage: `url("${props.background_image}")`
  };

  return (
    <a className="Card" href="#">
      <div className="Card__header">
        <div className="Card__background" style={style} />
        <div className="Card__meta">
          <img
            width="100"
            height="100"
            src={props.company_logo}
            alt={props.company_name}
          />
          {props.types.map(type => (
            <abbr
              className="Card__type"
              key={type}
              title="Enterprise Investment Scheme"
            >
              {type}
            </abbr>
          ))}
        </div>
      </div>
      <div className="Card__content">
        <h2 className="Card__title">{props.company_name}</h2>
        <p className="Card__description">{props.description}</p>
        <p className="Card__target">{'£' + props.target + ' Target'}</p>
        <Progress
          percentage={calculatePercentage(props.target, props.raised)}
        />
        <Stats />
      </div>
    </a>
  );
};

export default Card;
