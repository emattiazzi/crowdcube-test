import React, { Component } from 'react';
import Nav from './Nav/Nav';
import Filters from './Filters/Filters';
import Container from './Container/Container';
import List from './List/List';

import MOCK_DATA from './MOCK_DATA.json';


class App extends Component {
  render() {
    return (
      <div>
        <Container>
          <Nav />
        </Container>
        <Filters />
        <Container>
          <List items={MOCK_DATA} />
        </Container>
      </div>
    );
  }
}

export default App;
